/* run.config
   STDOPT: +"-lannot=FC -lannot-with-called=main,l"
 */

int f(){ return 0; }
int g(){ return 29; }
int h(){ return 13; }
int i(){ return h() + g(); }
int j(){ return f(); }
int k(){ return 0; }
int l(){ return 1; }

int main(){
  return i();
}
