/* Generated by Frama-C LTest */


#ifndef pc_label
#define pc_label(...) do{}while(0)
#endif
#ifndef pc_label_bindings
#define pc_label_bindings(...) do{}while(0)
#endif

int f(void)
 {
   int __retres;
   __retres = 0;
   return __retres;
 }

int g(void)
{
  int __retres;
  pc_label(1,1,"FC");
  __retres = 29;
  return __retres;
}

int h(void)
{
  int __retres;
  pc_label(1,2,"FC");
  __retres = 13;
  return __retres;
}

int i(void)
{
  int __retres;
  int tmp;
  int tmp_0;
  pc_label(1,3,"FC");
  tmp = h();
  tmp_0 = g();
  __retres = tmp + tmp_0;
  return __retres;
}

int j(void)
{
  int tmp;
  tmp = f();
  return tmp;
}

int k(void)
{
  int __retres;
  __retres = 0;
  return __retres;
}

int l(void)
{
  int __retres;
  pc_label(1,4,"FC");
  __retres = 1;
  return __retres;
}

int main(void)
{
  int tmp;
  pc_label(1,5,"FC");
  tmp = i();
  return tmp;
}


